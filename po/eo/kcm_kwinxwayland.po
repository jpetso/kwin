# translation of kcm_kwinxwayland.pot to esperanto
# Copyright (C) 2003 Free Software Foundation, Inc.
# This file is distributed under the same license as the kwin package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_kwinxwayland\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-29 01:38+0000\n"
"PO-Revision-Date: 2023-11-30 18:56+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ui/main.qml:47
#, kde-format
msgid ""
"Legacy X11 apps require the ability to read keystrokes typed in other apps "
"for features that are activated using global keyboard shortcuts. This is "
"disabled by default for security reasons. If you need to use such apps, you "
"can choose your preferred balance of security and functionality here."
msgstr ""
"Heredaĵaj X11-aplikoj postulas la kapablon legi klavpremojn tajpitajn en "
"aliaj apoj por funkcioj aktivigitaj per ĉieaj klavaraj ŝparvojoj. Ĉi tio "
"estas malŝaltita defaŭlte pro sekurecaj kialoj. Se vi bezonas uzi tiajn "
"programojn, vi povas elekti vian preferatan ekvilibron de sekureco kaj "
"funkcieco ĉi tie."

#: ui/main.qml:64
#, kde-format
msgid "Allow legacy X11 apps to read keystrokes typed in all apps:"
msgstr ""
"Permesu al heredaj X11-aplikoj legi klavopremojn tajpitajn en ĉiuj aplikaĵoj:"

#: ui/main.qml:65
#, kde-format
msgid "Never"
msgstr "Neniam"

#: ui/main.qml:68
#, kde-format
msgid "Only non-character keys"
msgstr "Nur ne-signajn klavojn"

#: ui/main.qml:71
#, kde-format
msgid ""
"As above, plus any key typed while the Control, Alt, or Meta keys are pressed"
msgstr ""
"Kiel supre, plus iu klavo tajpita dum kiam la Ctrl-, Alt- aŭ Meta-klavo "
"estas premata"

#: ui/main.qml:75
#, kde-format
msgid "Always"
msgstr "Ĉiam"

#: ui/main.qml:82
#, kde-format
msgid ""
"Note that using this setting will reduce system security to that of the X11 "
"session by permitting malicious software to steal passwords and spy on the "
"text that you type. Make sure you understand and accept this risk."
msgstr ""
"Notu, ke uzi ĉi tiun agordon reduktos sisteman sekurecon al tiu de la X11-"
"seanco permesante malican programaron ŝteli pasvortojn kaj spioni la "
"tekston, kiun vi tajpas. Certiĝu, ke vi komprenas kaj akceptas ĉi tiun "
"riskon."
